package com.zy.feign;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    //注入自己写的远程调用接口
    @Autowired
    MyFeignClient fc;


    @RequestMapping("/feign")
    public String getMesg(){

        return  fc.getMesg();


    }
}
