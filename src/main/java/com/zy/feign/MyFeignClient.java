package com.zy.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(value ="eureka-client")  //指向服务名称
public interface MyFeignClient {//Feign远程调用就是在这个接口中实现的

    @RequestMapping("/client")//请求路径必须和服务中的@RequestMapping("")一致
    public String getMesg();//和服务中的方法配套,调用的服务就会返回json字符串


    //http://eureka-client/client

}
